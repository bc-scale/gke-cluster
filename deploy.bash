#!/bin/bash
# file <deploy.bash>
set -Eeuo pipefail

set -x

traperr() {
  echo "ERROR: ${BASH_SOURCE[1]} at about line ${BASH_LINENO[0]}"
}

set -o errtrace
trap traperr ERR

# shellcheck disable=SC1091
source env.bash

validate_env () {
    if [[ -z ${PROJECT_ID+x} ]] || [[ -z ${REGION+x} ]]; then
      echo "To run this deployment you need to export PROJECT_ID and REGION as follows:
      export REGION=<region e.g. europe-west1>
      export PROJECT_ID=<project name e.g. kong>";
      exit 1
    fi
}

gcloud_setup () {
  gcloud config set project "$PROJECT_ID"
  # implicitly enable the apis we'll use
  gcloud services enable storage-api.googleapis.com
  gcloud services enable cloudresourcemanager.googleapis.com
  gcloud services enable compute.googleapis.com
  gcloud services enable container.googleapis.com
  gcloud services enable iam.googleapis.com

  # create a service account in the project, give it permissions, and obtain a key for terraform to use
  if gcloud iam service-accounts list | grep "terraform@$PROJECT_ID.iam.gserviceaccount.com" | awk '{print $1}'; then
    echo "service-accounts: terraform@$PROJECT_ID.iam.gserviceaccount.com"
  else
    gcloud iam service-accounts create terraform --display-name "terraform"
    gcloud projects add-iam-policy-binding "$PROJECT_ID" --member "serviceAccount:terraform@$PROJECT_ID.iam.gserviceaccount.com" --role "roles/owner"
  fi

  if [[ ! -f $GOOGLE_APPLICATION_CREDENTIALS_PATH ]]; then
    gcloud iam service-accounts keys create "$GOOGLE_APPLICATION_CREDENTIALS_PATH" --iam-account terraform@"$PROJECT_ID".iam.gserviceaccount.com
  fi

  # make the key available in this session
  export GOOGLE_APPLICATION_CREDENTIALS="$GOOGLE_APPLICATION_CREDENTIALS_PATH"

  # implicitly install other gcloud components we'll need to run the rest
  gcloud components install -q gsutil kubectl docker-credential-gcr
}

terraform_deployment () {
  # create the shared state bucket for terraform to save it being persisted locally / allow other people to run the tooling
  gsutil mb -l "$REGION" gs://"$PROJECT_ID"-terraform-state || :

  # initialise terraform state and providers
  terraform init -backend-config=bucket="$PROJECT_ID"-terraform-state

  # apply the .tf manifests i.e. create the infrastructure and the cluster
  terraform apply
}

set_context() {
  # get some creds for the cluster to use for the next bit
  gcloud container clusters get-credentials kong-cluster --region "$REGION"

  # explicitly specify the cluster in case we have others configured
  kubectl config use-context gke_"${PROJECT_ID}"_"${REGION}"_kong-cluster
}

init_cluster_resources () {
  # apply the kubernetes manifests to provision the namespaces we need
  kubectl create --save-config -f ./k8s/common/namespaces.yaml #|| :

  # apply the kubernetes manifests which are declarative:
  # generic k8s
  kubectl apply -f ./k8s/storage-classes || :
}

deploy_secrets () {
  secrets_tls_path=$SECRETS_BASE_PATH/tls

  # create a private key and a self signed certificate (remember that old skool 2048 bit as Google load balancers don't like the stronger RSA-4096)
  if [[ ! -f $secrets_tls_path/kong.hyperd.sh.key ]] || [[ ! -f $secrets_tls_path/kong.hyperd.sh.crt ]]; then
    openssl req -new -newkey rsa:4096 -days 365 -nodes -x509 \
    -subj "/C=NL/ST=Amsterdam/L=Amsterdam/O=hyperd/CN=kong.hyperd.sh" \
    -keyout "$secrets_tls_path"/kong.hyperd.sh.key -out "$secrets_tls_path"/kong.hyperd.sh.crt

    # make sure to remove the secret kong-tls, if exists
    kubectl -n kong delete secret kong-tls || :
  fi

  # create a strong Diffie-Hellman group, used in negotiating Perfect Forward Secrecy with clients
  if [[ ! -f $secrets_tls_path/dhparam.pem ]]; then
    openssl dhparam -out "$secrets_tls_path"/dhparam.pem 2048

    # make sure to remove the secret kong-tls-dhparam, if exists
    kubectl -n kong delete secret kong-tls-dhparam || :
  fi

  # use an imperative command to create a kubernetes secret from this key that can be used with the GCE ingress
  tls_secret=$(kubectl -n kong get secrets | awk '{print $1}' | awk -F, '$1 == V' V="kong-tls")
  if [[ -z "$tls_secret" ]]; then
    kubectl -n kong create secret tls kong-tls --key "$secrets_tls_path"/kong.hyperd.sh.key --cert "$secrets_tls_path"/kong.hyperd.sh.crt
  fi

  tls_dhparam_secret=$(kubectl -n kong get secrets | awk '{print $1}' | awk -F, '$1 == V' V="kong-tls-dhparam")
  if [[ -z "$tls_dhparam_secret" ]]; then
    kubectl -n kong create secret generic kong-tls-dhparam --from-file="$secrets_tls_path"/dhparam.pem
  fi
}

init() {

  # check that PROJECT_ID and REGION are exported in the current shell
  validate_env

  # run the initial gcloud setup
  gcloud_setup

  # deploy the k8s cluster with terraform
  terraform_deployment

  # configure the cluster context for kubectl
  set_context

  # deploy the necessary initial resources to our GKE cluster
  init_cluster_resources
}

init
