config {
  module = true
  deep_check = false
  force = true

  varfile = ["terraform.tfvars"]
}
